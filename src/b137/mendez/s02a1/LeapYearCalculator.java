package b137.mendez.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args) {
        System.out.println("Leap Year Calculator\n");
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your firstname?\n");
        String firstName = appScanner.nextLine();
        System.out.println("Hello, " + firstName + "!\n");

        // Activity: Create a program that check if a year is a leap year or not.
        System.out.println("Enter the year to be checked.\n");
        int yearToCheck = appScanner.nextInt();
        // System.out.println("Year, " + yearToCheck + "!\n");
        boolean leapYear = false;

        // To check Leap Year
        // 1. If the year is evenly divisible by 4, go to step 2.
        if (yearToCheck % 4 == 0) {
            // 2. If the year is evenly divisible by 100, go to step 3.
            if (yearToCheck % 100 == 0) {
                leapYear = true;
                // 3. If the year is evenly divisible by 400, go to step 4.
                if (yearToCheck % 400 == 0) {
                    // 4. The year is a leap year (it has 366 days).
                    leapYear = true;
                }
                else leapYear = false;
            }
            else leapYear = true;
        }
        // 5. The year is not a leap year (it has 365 days).
        else leapYear = false;

        if (leapYear) {
            System.out.println(yearToCheck + " is a leap year!");
        }
        else {
            System.out.println(yearToCheck + " is not a leap year!");
        }

    }
}
